import cv2

image = cv2.imread("1.1.jpg")



image2 = cv2.imread("1.2.jpg")
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)  # grayscale

gray2 = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)  # grayscale

_, thresh = cv2.threshold(gray, 100, 255, cv2.THRESH_BINARY_INV)
_, thresh = cv2.threshold(gray2, 100, 255, cv2.THRESH_BINARY_INV)

# threshold
kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
dilated = cv2.dilate(thresh, kernel, iterations=13)  # dilate
contour, contours, _ = cv2.findContours(gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
contour2, contours2, _ = cv2.findContours(gray2, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
# get contours
count1 = 0
count2 = 0
k = 0
listB = list()
listG = list()
listR = list()

listB2 = list()
listG2 = list()
listR2 = list()

for contour in contours:
    # get rectangle bounding contour
    [x, y, w, h] = cv2.boundingRect(contour)
    # discard areas that are too large
    if h > 300 and w > 300:
        continue
    # discard areas that are too small
    if h < 40 or w < 40:
        continue
    # draw rectangle around contour on original image
    cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 255), 2)
    roi = image[y:y + h, x:x + w]

    sumB = 0
    sumG = 0
    sumR = 0

    height, width = roi.shape[:2]

    for i in range(0, height - 1):
        for j in range(0, width - 1):
            b = roi[i, j, 0]
            g = roi[i, j, 1]
            r = roi[i, j, 2]
            sumB = sumB + b
            sumG = sumG + g
            sumR = sumR + r

    averageB = int(sumB / (height * width))
    averageG = int(sumG / (height * width))
    averageR = int(sumR / (height * width))
    listB.append(averageB)
    listG.append(averageG)
    listR.append(averageR)

    print("Contour, shape's average value Blue " + str(count1)+"  "+str(listB[count1]))
    print("Contour, shape's average value Green" + str(count1)+"  "+str(listG[count1]))
    print("Contour, shape's average value Red " + str(count1)+"  "+str(listR[count1]))
    count1 = count1+1
cv2.imshow("contoured", image)
print ("Second image..... \n")
for contour2 in contours2:
    # get rectangle bounding contour
    [x, y, w, h] = cv2.boundingRect(contour2)
    # [x, y, w, h] = cv2.boundingRect(contour2)
    # discard areas that are too large
    if h > 300 and w > 300:
        continue
    # discard areas that are too small
    if h < 40 or w < 40:
        continue
    # draw rectangle around contour on original image
    cv2.rectangle(image2, (x, y), (x + w, y + h), (255, 0, 255), 2)
    roi2 = image2[y:y + h, x:x + w]

    sumB2 = 0
    sumG2 = 0
    sumR2 = 0
    height, width = roi2.shape[:2]

    for i in range(0, height - 1):
        for j in range(0, width - 1):
            b = roi2[i, j, 0]
            g = roi2[i, j, 1]
            r = roi2[i, j, 2]
            sumB2 = sumB2 + b
            sumG2 = sumG2 + g
            sumR2 = sumR2 + r
    averageB2 = int(sumB2 / (height * width))
    averageG2 = int(sumG2 / (height * width))
    averageR2 = int(sumR2 / (height * width))
    listB2.append(averageB2)
    listG2.append(averageG2)
    listR2.append(averageR2)

    print("Contour, shape's average value Blue " + str(count2) + "  " + str(listB2[count2]))
    print("Contour, shape's average value Green" + str(count2) + "  " + str(listG2[count2]))
    print("Contour, shape's average value Red " + str(count2) + "  " + str(listR2[count2]))
    count2 = count2 + 1

cv2.imshow("contoured2", image2)
print (listB)
print(listB2)
i = 0
difference = list()
for i in range(0, len(listB)-1):
    Subs = int(listB[i] - listB2[i])

    difference.append(abs(Subs))
print(difference)
cv2.waitKey(0)



