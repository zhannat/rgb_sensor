import cv2
import numpy as np

image = cv2.imread("contoured0.jpg")
cv2.imshow("origin", image)
#cv2.imshow("hsl", hsl)

#b = image[100, 100, 0]
#g = image[100, 100, 1]
#r = image[100, 100, 2]
height, width = image.shape[:2]
sumB = 0
sumG = 0
sumR = 0
for i in range(0, height-1):
    for j in range(0, width-1):
        b = image[i, j, 0]
        g = image[i, j, 1]
        r = image[i, j, 2]
        sumB = sumB + b
        sumG = sumG + g
        sumR = sumR + r

print(sumB)
averageB = sumB/(height*width)
averageG = sumG/(height*width)
averageR = sumR/(height*width)

print(int(averageB), int(averageG), int(averageR))
#print (b,g,r)
cv2.waitKey(0)