import cv2
import numpy as np
import time

cap = cv2.VideoCapture(0)
time.sleep(2) #Creating delay for camera to be set and focused for the reference shot
rate, img = cap.read()

# Creating regions of interests(ROI), with 9 regions whereas they are related to the 9 sensing elements on camera
ROI = [img[144:180, 218:256], img[140:180, 320:360], img[150:180, 430:460],
       img[225:260, 220:250], img[230:255, 325:355], img[225:270, 415:460],
       img[315:347, 215:247], img[320:345, 320:350], img[320:345, 415:450]]

k = 0
list1 = list() # List 1 will store values, which correspond to the HSV value of intensity in the reference shot
list2 = list() # List 2 will store values, which correspond to the HSV value of intensity in the comparison shot

# Starting the loop, where the reference shot's values will be calculated
for i in range(9):
    roi = ROI[i]
    hsv2 = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)
    height, width = roi.shape[:2]
    Sum = 0
    for i in range(0, height - 1):
        for j in range(0, width - 1):
            v = hsv2[i, j, 2]
            Sum = Sum + v

    average = int(Sum / (height * width))
    list1.append(average)

# Declaring pass command for contact localization frame window
def nothing(x):
        pass

# Creating contact localization window
contact = np.zeros((300, 300, 3), np.uint8)
cv2.namedWindow('contact')
switch = 'On/off'
cv2.createTrackbar(switch, 'contact', 0, 1, nothing)

# Starting loop, which iterates each frame and sent through the intensity values extraction process
while(True):
    ret, img2 = cap.read()
    cv2.imshow('img2', img2)
    #print(img2.shape)
    ROI2 = [img2[144:180, 218:256], img2[140:180, 320:360], img2[150:180, 430:460],
            img2[225:260, 220:250], img2[230:255, 325:355], img2[225:270, 415:460],
            img2[315:347, 215:247], img2[320:345, 320:350], img2[320:345, 415:450]]

    cv2.imshow("contact", contact)
    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break
    for i in range(9):
        roi2 = ROI2[i]
        hsv2 = cv2.cvtColor(roi2, cv2.COLOR_BGR2HSV)
        height, width = roi2.shape[:2]
        Sum = 0
        #print(height, width)
        for i in range(0, height - 1):
            for j in range(0, width - 1):
                v = hsv2[i, j,2]
                Sum = Sum + v

        average2 = int(Sum / (height * width))
        list2.append(average2)
        #list2[i] = average2
        k += 1

    i = 0
    difference = list()
    for i in range(len(list1)):
        Subs = int(list1[i] - list2[i])
        difference.append(abs(Subs))
    print(difference)

    # Clearing list for new iteration
    list2.clear()

    # Piece of code corresponding to contact location, which is filled in with the difference values and display contact location
    Zones = {0: [0, 100, 0, 100, difference[0] * 2],
         1: [100, 200, 0, 100, difference[1] * 2],
         2: [200, 300, 0, 100, difference[2] * 2],
         3: [0, 100, 100, 200, difference[3] * 2],
         4: [100, 200, 100, 200, difference[4] * 2],
         5: [200, 300, 100, 200, difference[5] * 2],
         6: [0, 100, 200, 300, difference[6] * 2],
         7: [100, 200, 200, 300, difference[7] * 2],
         8: [200, 300, 200, 300, difference[8] * 2]
         }

    s = cv2.getTrackbarPos(switch, 'contact')

    if s == 0:
        contact[:] = 0
    elif s == 1:
        for zone in Zones:
            for y in range(Zones[zone][0], Zones[zone][1]):
                for x in range(Zones[zone][2], Zones[zone][3]):
                    contact[y, x] = [Zones[zone][4], Zones[zone][4], Zones[zone][4]]

    # print(difference.index(max(difference)))
cv2.waitKey(0)

