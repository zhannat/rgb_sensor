import numpy as np
import cv2
from matplotlib import pyplot as plt
cap = cv2.VideoCapture(0)

while(1):

    # Take each frame
    _, frame = cap.read()
    # color = ('b','g','r')
    # for i, col in enumerate(color):
    #     histr = cv2.calcHist([frame],[i],None,[256],[0,256])
    #     plt.plot(histr, color = col)
    #     plt.xlim([0, 256])
    # # plt.show()


    # #second approach
    # avg_color_per_row = np.average(frame, axis=None)
    # avg_color = np.average(avg_color_per_row, axis=None)
    # print(avg_color)

    # np.mean calculates mean values along height and width
    # returns these values
    avg1_color = np.mean(frame, axis=(0, 1))
    print(avg1_color)
    #cv2.imshow("frame",frame)

cv2.destroyAllWindows()