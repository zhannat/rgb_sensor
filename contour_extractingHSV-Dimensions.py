import cv2
import numpy as np

img = cv2.imread("Experiments/Red/Red_1.jpg")
img2 = cv2.imread("Experiments/Red/Red_5.jpg")

# get contours
count1 = 0
count2 = 0


ROI = [img[270:400, 670:820], img[490:620, 670:820], img[700:825, 670:800],
       img[280:430, 930:1060], img[530:630, 930:1040], img[720:820, 930:1020],
       img[330:430, 1160:1270], img[520:660, 1130:1280], img[740:825, 1140:1250]]

ROI2 = [img2[270:400, 670:820], img2[490:620, 670:820], img2[700:825, 670:800],
       img2[280:430, 930:1060], img2[530:630, 930:1040], img2[720:820, 930:1020],
       img2[330:430, 1160:1270], img2[520:660, 1130:1280], img2[740:825, 1140:1250]]


k = 0
list1 = list()
list2 = list()

for i in range(9):
    roi = ROI[i]
    hsv2 = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)
    height, width = roi.shape[:2]
    Sum = 0
    for i in range(0, height - 1):
        for j in range(0, width - 1):
            v = hsv2[i, j, 2]
            Sum = Sum + v
    # print(v)
    average = int(Sum / (height * width))
    list1.append(average)
    print("Contour, shapes average value" + str(count1)+"  " +str(list1[count1]))

    cv2.imshow("contoured", roi)
    cv2.imshow("Initial" +str(i)+".jpg", roi)
    count1 = count1+1

for i in range(9):
    roi2 = ROI2[i]
    hsv2 = cv2.cvtColor(roi2, cv2.COLOR_BGR2HSV)
    height, width = roi2.shape[:2]
    Sum = 0

    for i in range(0, height - 1):
        for j in range(0, width - 1):
            v = hsv2[i, j, 2]
            Sum = Sum + v
    # print(v)
    #print(Sum)
    average2 = int(Sum / (height * width))
    list2.append(average2)
    k += 1
    print("Contour2, shape" + str(count2)+ "  " +str(list2[count2]))

    # cv2.rectangle(image2, (x, y), (x + w, y + h), (255, 0, 255), 2)
    # roi = image2[y:y + h, x:x + w]
    #cv2.imshow("contoured2", image2)
    cv2.imshow("Initial" + str(i) + ".jpg", roi2)
    count2 = count2 + 1
print (list1)
print(list2)
i = 0
difference = list()
for i in range(len(list1)):
    Subs = int(list1[i] - list2[i])

    difference.append(abs(Subs))
print(difference)

#printing out average values in deltaI (difference in intensity)
av_diff = np.average(difference)
print(av_diff)

Zones = {0: [0, 100, 0, 100, difference[0]*2],
         1: [100, 200, 0, 100, difference[1]*2],
         2: [200, 300, 0, 100, difference[2]*2],
         3: [0, 100, 100, 200, difference[3]*2],
         4: [100, 200, 100, 200, difference[4]*2],
         5: [200, 300, 100, 200, difference[5]*2],
         6: [0, 100, 200, 300, difference[6]*2],
         7: [100, 200, 200, 300, difference[7]*2],
         8: [200, 300, 200, 300, difference[8]*2]
         }


def nothing(x):
    pass


# Create a black image, a window
contact = np.zeros((300,300,3), np.uint8)

cv2.namedWindow('contact')
switch = 'On/off'
cv2.createTrackbar(switch, 'contact', 0, 1, nothing)
print(difference.index(max(difference)))

while(1):
    cv2.imshow("contact", contact)
    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break
    s = cv2.getTrackbarPos(switch, 'contact')

    if s == 0:
        contact[:] = 0
    elif s == 1 :
        for zone in Zones:
            for y in range(Zones[zone][0], Zones[zone][1]):
                for x in range(Zones[zone][2], Zones[zone][3]):
                    contact[y, x] = [Zones[zone][4], Zones[zone][4], Zones[zone][4]]

cv2.waitKey(0)
cv2.destroyAllWindows()

