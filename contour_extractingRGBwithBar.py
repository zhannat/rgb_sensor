import cv2
import numpy as np
image = cv2.imread("1.1.jpg")
image2 = cv2.imread("1.2.jpg")
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)  #

gray2 = cv2.cvtColor(image2, cv2.COLOR_BGR2GRAY)  # grayscale

_, thresh = cv2.threshold(gray, 100, 255, cv2.THRESH_BINARY_INV)
_, thresh = cv2.threshold(gray2, 100, 255, cv2.THRESH_BINARY_INV)

# threshold
kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
dilated = cv2.dilate(thresh, kernel, iterations=13)  # dilate
contour, contours, _ = cv2.findContours(gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
contour2, contours2, _ = cv2.findContours(gray2, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
# get contours
count1 = 0
count2 = 0
k = 0
listB = list()
listG = list()
listR = list()

listB2 = list()
listG2 = list()
listR2 = list()



for contour in contours:
    # get rectangle bounding contour
    [x, y, w, h] = cv2.boundingRect(contour)
    # discard areas that are too large
    if h > 300 and w > 300:
        continue
    # discard areas that are too small
    if h < 40 or w < 40:
        continue
    # draw rectangle around contour on original image
    cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 255), 2)
    roi = image[y:y + h, x:x + w]

    sumB = 0
    sumG = 0
    sumR = 0

    height, width = roi.shape[:2]

    for i in range(0, height - 1):
        for j in range(0, width - 1):
            b = roi[i, j, 0]
            g = roi[i, j, 1]
            r = roi[i, j, 2]
            sumB = sumB + b
            sumG = sumG + g
            sumR = sumR + r

    averageB = int(sumB / (height * width))
    averageG = int(sumG / (height * width))
    averageR = int(sumR / (height * width))
    listB.append(averageB)
    listG.append(averageG)
    listR.append(averageR)

    print("Contour, shape's average value Blue " + str(count1)+"  "+str(listB[count1]))
    print("Contour, shape's average value Green" + str(count1)+"  "+str(listG[count1]))
    print("Contour, shape's average value Red " + str(count1)+"  "+str(listR[count1]))
    count1 = count1+1
cv2.imshow("contoured", image)
print ("Second image..... \n")
for contour2 in contours2:
    # get rectangle bounding contour
    [x, y, w, h] = cv2.boundingRect(contour2)
    # [x, y, w, h] = cv2.boundingRect(contour2)
    # discard areas that are too large
    if h > 300 and w > 300:
        continue
    # discard areas that are too small
    if h < 40 or w < 40:
        continue
    # draw rectangle around contour on original image
    cv2.rectangle(image2, (x, y), (x + w, y + h), (255, 0, 255), 2)
    roi2 = image2[y:y + h, x:x + w]

    sumB2 = 0
    sumG2 = 0
    sumR2 = 0
    height, width = roi2.shape[:2]

    for i in range(0, height - 1):
        for j in range(0, width - 1):
            b = roi2[i, j, 0]
            g = roi2[i, j, 1]
            r = roi2[i, j, 2]
            sumB2 = sumB2 + b
            sumG2 = sumG2 + g
            sumR2 = sumR2 + r
    averageB2 = int(sumB2 / (height * width))
    averageG2 = int(sumG2 / (height * width))
    averageR2 = int(sumR2 / (height * width))
    listB2.append(averageB2)
    listG2.append(averageG2)
    listR2.append(averageR2)

    print("Contour, shape's average value Blue " + str(count2) + "  " + str(listB2[count2]))
    print("Contour, shape's average value Green" + str(count2) + "  " + str(listG2[count2]))
    print("Contour, shape's average value Red " + str(count2) + "  " + str(listR2[count2]))
    count2 = count2 + 1

cv2.imshow("contoured2", image2)
print (listB)
print(listB2)
i = 0
differenceB = list()
differenceG = list()
differenceR = list()

for i in range(0, len(listB)-1):
    SubsB = int(listB[i] - listB2[i])
    SubsG = int(listG[i] - listG2[i])
    SubsR = int(listR[i] - listR2[i])

    differenceB.append(abs(SubsB))
    differenceG.append(abs(SubsG))
    differenceR.append(abs(SubsR))

print(differenceB, differenceG, differenceR)


Zones = {0: [0, 100, 0, 100, differenceB[0]*2, differenceG[0]*2, differenceR[0]*2],
         1: [100, 200, 0, 100, differenceB[1]*2, differenceG[1]*2, differenceR[1]*2],
         2: [200, 300, 0, 100, differenceB[2]*2, differenceG[2]*2, differenceR[2]*2],
         3: [0, 100, 100, 200, differenceB[3]*2, differenceG[3]*2, differenceR[3]*2],
         4: [100, 200, 100, 200, differenceB[4]*2, differenceG[4]*2, differenceR[4]*2],
         5: [200, 300, 100, 200, differenceB[5]*2, differenceG[5]*2, differenceR[5]*2],
         6: [0, 100, 200, 300, differenceB[6]*2, differenceG[6]*2, differenceR[6]*2],
         7: [100, 200, 200, 300, differenceB[7]*2, differenceG[7]*2, differenceR[7]*2],
         }
#8: [200, 300, 200, 300, differenceB[8] * 2, differenceG[8] * 2, differenceR[8] * 2]


def nothing(x):
    pass


# Create a black image, a window
img = np.zeros((300,300,3), np.uint8)
cv2.namedWindow('image')

switch = 'On/off'
cv2.createTrackbar(switch, 'image', 0, 1, nothing)
while(1):
    cv2.imshow('image',img)
    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break
    # get current positions of four trackbars
    #r = cv2.getTrackbarPos('R','image')
    #g = cv2.getTrackbarPos('G','image')
    #b = cv2.getTrackbarPos('B','image')
    s = cv2.getTrackbarPos(switch, 'image')

    if s == 0:
        img[:] = 0
    elif s == 1:
        for zone in Zones:
            for y in range(Zones[zone][0], Zones[zone][1]):
                for x in range(Zones[zone][2], Zones[zone][3]):
                    img[y, x] = [Zones[zone][4], Zones[zone][5], Zones[zone][6]]

cv2.waitKey(0)



