import cv2
import numpy as np
image = cv2.imread("1.1.jpg")
image2 = cv2.imread("1.2.jpg")
img = cv2.imread("try.jpg")
img2 = cv2.imread("try2.jpg")

# get contours
count1 = 0
count2 = 0
k = 0
listB = list()
listG = list()
listR = list()

listB2 = list()
listG2 = list()
listR2 = list()



ROI = [img[270:400, 670:820], img[490:620, 670:820], img[700:825, 670:800],
       img[280:430, 930:1060], img[530:630, 930:1040], img[720:820, 930:1020],
       img[330:430, 1160:1270], img[520:660, 1130:1280], img[740:825, 1140:1250]]

ROI2 = [img2[270:400, 670:820], img2[490:620, 670:820], img2[700:825, 670:800],
       img2[280:430, 930:1060], img2[530:630, 930:1040], img2[720:820, 930:1020],
       img2[330:430, 1160:1270], img2[520:660, 1130:1280], img2[740:825, 1140:1250]]

for i in range(9):
    roi = ROI[i]
    sumB = 0
    sumG = 0
    sumR = 0
    height, width = roi.shape[:2]

    for i in range(0, height - 1):
        for j in range(0, width - 1):
            b = roi[i, j, 0]
            g = roi[i, j, 1]
            r = roi[i, j, 2]
            sumB = sumB + b
            sumG = sumG + g
            sumR = sumR + r

    averageB = int(sumB / (height * width))
    averageG = int(sumG / (height * width))
    averageR = int(sumR / (height * width))
    listB.append(averageB)
    listG.append(averageG)
    listR.append(averageR)

    print("Contour, shape's average value Blue " + str(count1)+"  "+str(listB[count1]))
    print("Contour, shape's average value Green" + str(count1)+"  "+str(listG[count1]))
    print("Contour, shape's average value Red " + str(count1)+"  "+str(listR[count1]))
    count1 = count1+1
print ("Second image..... \n")


for k in range(9):

    roi2 = ROI2[k]
    sumB2 = 0
    sumG2 = 0
    sumR2 = 0
    height, width = roi2.shape[:2]

    for i in range(0, height - 1):
        for j in range(0, width - 1):
            b = roi2[i, j, 0]
            g = roi2[i, j, 1]
            r = roi2[i, j, 2]
            sumB2 = sumB2 + b
            sumG2 = sumG2 + g
            sumR2 = sumR2 + r
    averageB2 = int(sumB2 / (height * width))
    averageG2 = int(sumG2 / (height * width))
    averageR2 = int(sumR2 / (height * width))
    listB2.append(averageB2)
    listG2.append(averageG2)
    listR2.append(averageR2)

    print("Contour, shape's average value Blue " + str(count2) + "  " + str(listB2[count2]))
    print("Contour, shape's average value Green" + str(count2) + "  " + str(listG2[count2]))
    print("Contour, shape's average value Red " + str(count2) + "  " + str(listR2[count2]))
    count2 = count2 + 1

print(listB)
print(listB2)
differenceB = list()
differenceG = list()
differenceR = list()

for i in range(len(listB)):
    SubsB = int(listB[i] - listB2[i])
    SubsG = int(listG[i] - listG2[i])
    SubsR = int(listR[i] - listR2[i])

    differenceB.append(abs(SubsB))
    differenceG.append(abs(SubsG))
    differenceR.append(abs(SubsR))

print(differenceB, differenceG, differenceR)


Zones = {0: [0, 100, 0, 100, differenceB[0], differenceG[0], differenceR[0]],
         1: [100, 200, 0, 100, differenceB[1], differenceG[1], differenceR[1]],
         2: [200, 300, 0, 100, differenceB[2], differenceG[2], differenceR[2]],
         3: [0, 100, 100, 200, differenceB[3], differenceG[3], differenceR[3]],
         4: [100, 200, 100, 200, differenceB[4], differenceG[4], differenceR[4]],
         5: [200, 300, 100, 200, differenceB[5], differenceG[5], differenceR[5]],
         6: [0, 100, 200, 300, differenceB[6], differenceG[6], differenceR[6]],
         7: [100, 200, 200, 300, differenceB[7], differenceG[7], differenceR[7]],
         8: [200, 300, 200, 300, differenceB[8], differenceG[8], differenceR[8]]
         }


def nothing(x):
    pass


# Create a black image, a window
contact = np.zeros((300,300,3), np.uint8)
cv2.namedWindow('contact')

switch = 'On/off'
cv2.createTrackbar(switch, 'contact', 0, 1, nothing)

while(1):
    cv2.imshow("contact",contact)
    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break
    s = cv2.getTrackbarPos(switch, 'contact')

    if s == 0:
        contact[:] = 0
    elif s == 1:
        for zone in Zones:
            for y in range(Zones[zone][0], Zones[zone][1]):
                for x in range(Zones[zone][2], Zones[zone][3]):
                    contact[y, x] = [Zones[zone][4], Zones[zone][5], Zones[zone][6]]

cv2.waitKey(0)
cv2.destroyAllWindows()



