import cv2
import numpy as np

def nothing(x):
    pass

# Create a black image, a window
img = np.zeros((300,300,3), np.uint8)
cv2.namedWindow('image')

# create trackbars for color change
#cv2.createTrackbar('R','image',0,255,nothing)
#cv2.createTrackbar('G','image',0,255,nothing)
#cv2.createTrackbar('B','image',0,255,nothing)

# create switch for ON/OFF functionality
switch = 'On/off'
cv2.createTrackbar(switch, 'image', 0, 1, nothing)
listB = list()
listG = list()
listR = list()

listB = [20, 30, 40, 50, 60, 70, 80, 90, 60]
listG = [20, 130, 140, 150, 160, 170, 180, 190, 160]
listR = [220, 220, 35, 70, 20, 150, 180, 20, 220]


Zones = {0: [0, 100, 0, 100, listB[0], listG[0], listR[0]],
         1: [100, 200, 0, 100, listB[1], listG[1], listR[1]],
         2: [200, 300, 0, 100, listB[2], listG[2], listR[2]],
         3: [0, 100, 100, 200, listB[3], listG[3], listR[3]],
         4: [100, 200, 100, 200, listB[4], listG[4], listR[4]],
         5: [200, 300, 100, 200, listB[5], listG[5], listR[5]],
         6: [0, 100, 200, 300, listB[6], listG[6], listR[6]],
         7: [100, 200, 200, 300, listB[7], listG[7], listR[7]],
         8: [200, 300, 200, 300, listB[8], listG[8], listR[8]]
         }


while(1):
    cv2.imshow('image',img)
    k = cv2.waitKey(1) & 0xFF
    if k == 27:
        break
    # get current positions of four trackbars
    #r = cv2.getTrackbarPos('R','image')
    #g = cv2.getTrackbarPos('G','image')
    #b = cv2.getTrackbarPos('B','image')
    s = cv2.getTrackbarPos(switch, 'image')

    if s == 0:
        img[:] = 0
    elif s == 1:
        for zone in Zones:
            for y in range(Zones[zone][0], Zones[zone][1]):
                for x in range(Zones[zone][2], Zones[zone][3]):
                    img[y, x] = [Zones[zone][4], Zones[zone][5], Zones[zone][6]]


cv2.waitKey(0)
cv2.destroyAllWindows()