import cv2
import numpy as np
from matplotlib import pyplot as plt

cap = cv2.VideoCapture(0)  # For choosing another camera try to use 1/0/-1

while  True:
    _, frame = cap.read()
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    lower_red = np.array([150,150,0])
    upper_red = np.array([180,250,250])

    # mask = cv2.inRange(hsv, lower_red, upper_red)
    # res = cv2.bitwise_and(frame, frame, mask = mask)

    h = hsv[100, 100, 0]
    s = hsv[100, 100, 1]
    v = hsv[100, 100, 2]


    b = frame[100, 100, 0]
    g = frame[100, 100, 1]
    r = frame[100, 100, 2]

    k = cv2.waitKey(5) & 0xFF

    if k == 32:
        print (b,g,r)

    cv2.imshow('frame', frame)
    # cv2.imshow('hsv', hsv)
    # cv2.imshow('res', res)
    color = ('b', 'g', 'r')
    a = 1
    for i, col in enumerate(color):
     histr = cv2.calcHist([frame], [i], None, [256], [15, 245])
     # print(histr)
     average = histr
     avg1_color = np.mean(average, axis=(0, 1))

     print(avg1_color)
     a = a+1
     plt.plot(histr, color=col)
     plt.xlim([0, 256])
    plt.show()

    avg1_color = np.average(histr, axis=(0, 1))

    print (avg1_color)
    if k == 27:
        break


#cv2.imshow('wRed',img)
#cv2.imshow('Red',r)
#cv2.imshow('Blue',b)
#cv2.imshow('Green',g)

cv2.destroyAllWindows()
cap.release()
