import cv2
from matplotlib import pyplot as plt

img = cv2.imread('1.1.jpg')
#gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

color = ('b', 'g', 'r')


for i, col in enumerate(color):
    histr = cv2.calcHist([img], [i], None, [256], [15, 245])
    plt.plot(histr, color=col)
    plt.xlim([0, 256])
    cv2.imshow("pic", img)
    cv2.waitKey(0)
plt.show()

cv2.imshow("pic", img)
cv2.destroyAllWindows()

