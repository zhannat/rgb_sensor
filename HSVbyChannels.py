import cv2
import numpy as np

# define range of  colors in HSV

image = cv2.imread("1.jpg")
ret, thresh1 = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY)
hsv2 = cv2.cvtColor(thresh1, cv2.COLOR_RGB2HSV)

hsv = cv2.cvtColor(image, cv2.COLOR_RGB2HSV)
lower_b = np.array([110, 50, 50])
upper_b = np.array([130, 255, 255])
blue = cv2.inRange(hsv, lower_b, upper_b)

lower_r = np.array([0, 100, 0])
upper_r = np.array([5, 255, 255])
red = cv2.inRange(hsv, lower_r, upper_r)

lower_g = np.array([50, 60, 60])  # lower bound
upper_g = np.array([100, 255, 255])
green = cv2.inRange(hsv, lower_g, upper_g)

# image2 = cv2.imread("RGB.jpg")
# hsv2 = cv2.cvtColor(image2, cv2.COLOR_BGR2HSV)

h = hsv2[100, 100, 0]
s = hsv2[100, 100, 1]
v = hsv2[100, 100, 2]

print(h, s, v)

cv2.imshow("origin", image)
cv2.imshow("hsv", hsv)
cv2.imshow("threshold",thresh1)

##cv2.imshow("Blue Color", blue)
# cv2.imshow("Red Color", red)
# cv2.imshow("Green Color", green)

#cv2.imwrite('Origin1.png', image)
#cv2.imwrite("hsv1.png", hsv)

#cv2.imwrite("Blue1.png", blue)
#cv2.imwrite("Red1.png", red)
#cv2.imwrite("Green1.png", green)
cv2.waitKey(0)
