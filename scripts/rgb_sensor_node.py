#!/usr/bin/env python
from __future__ import print_function
import roslib
import sys
import rospy
import cv2
from std_msgs.msg import String
from std_msgs.msg import Float64MultiArray

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

#==========students=============

import cv2
import numpy as np
import time

sys.setrecursionlimit(10000)

class image_converter:
  list1 = list() # List 1 will store values, which correspond to the HSV value of intensity in the reference shot
  list2 = list() # List 2 will store values, which correspond to the HSV value of intensity in the comparison shot
  list3 = list() # List 3 for BGR reference
  list4 = list() # List 4 for comparison value
  is_first_time = 0
  difference = list()
  bgr_difference = list()
  def __init__(self):
    self.feat_pub = rospy.Publisher("/image_feature", Float64MultiArray, queue_size=10)
    self.image_features = Float64MultiArray()
    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("/camera/image_raw",Image,self.callback)
     #   self.image_pub = rospy.Publisher("/image_topic_2",Image)

  ## Declaring pass command for contact localization frame window
  def nothing(x):
    pass
  # Creating contact localization window
  contact = np.zeros((300, 300, 3), np.uint8)
  cv2.namedWindow('contact')
  switch = 'On/off'
  cv2.createTrackbar(switch, 'contact', 0, 1, nothing)
  def callback(self,data):
    try:
      #cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
      cv_image = self.bridge.imgmsg_to_cv2(data)

    except CvBridgeError as e:
      print(e)

    #(rows,cols,channels) = cv_image.shape
    #if cols > 60 and rows > 60 :
      #cv2.circle(cv_image, (50,50), 10, 255)

    #cv2.imshow("Image window", cv_image)
    #cv2.waitKey(3)
    ROI = [cv_image[130:180, 190:240], cv_image[135:190, 300:350], cv_image[144:185, 400:448],
	   cv_image[221:275, 195:250], cv_image[221:268, 293:348], cv_image[226:275, 400:450],
	   cv_image[315:371, 192:237], cv_image[315:365, 291:351], cv_image[310:363, 396:447]]
    if self.is_first_time == 0:
      k = 0
      
      # Starting the loop, where the reference shot's values will be calculated
      for i in range(9):
	roi = ROI[i]
	hsv2 = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)
	height, width = roi.shape[:2]
	Sum = 0
	# Extracting BGR value for reference shot
	avg_clr_row = np.average(roi, axis=0)
	avg_clr = np.average(avg_clr_row, axis=0)
	self.list3.append(avg_clr)
	for i in range(0, height - 1):
	  for j in range(0, width - 1):
	    v = hsv2[i, j, 2]
	    Sum = Sum + v
	average = int(Sum / (height * width))
	self.list1.append(average)
    
    #cv2.imshow('roi1', ROI[1])
    #cv2.imshow('roi2', ROI[2])
    #cv2.imshow('roi3', ROI[3])
    #cv2.imshow('roi4', ROI[4])
    #cv2.imshow('roi5', ROI[5])
    #cv2.imshow('roi6', ROI[6])
    #cv2.waitKey(3)
    #print(self.list1)
    self.is_first_time = 1
    #cv2.imshow('img2', cv_image)
    #cv2.waitKey(3)
    #cv2.imshow("contact", self.contact)
    #k = cv2.waitKey(1) & 0xFF
    #if k == 27:
      #break
    ROI2 = [cv_image[130:180, 190:240], cv_image[135:190, 300:350], cv_image[144:185, 400:448],
	    cv_image[221:275, 195:250], cv_image[221:268, 293:348], cv_image[226:275, 400:450],
	    cv_image[315:371, 192:237], cv_image[315:365, 291:351], cv_image[310:363, 396:447]]

    for i in range(9):
      roi2 = ROI2[i]
      #cv2.imshow("ROI", roi2)
      #cv2.waitKey(3)
      hsv2 = cv2.cvtColor(roi2, cv2.COLOR_BGR2HSV)
      height, width = roi2.shape[:2]
      Sum = 0
      # comparison value with reference BGR shot
      avg_clr_row2 = np.average(roi2, axis=0)
      avg_clr2 = np.average(avg_clr_row2, axis=0)
      self.list4.append(avg_clr2)
      for i in range(0, height - 1):
	for j in range(0, width - 1):
	  v = hsv2[i, j,2]
          Sum = Sum + v
	average2 = int(Sum / (height * width))
      self.list2.append(average2)
      #list2[i] = average2
      #k += 1
    #print(self.list2)  
    #cv2.imshow("roi2", roi2)
    #cv2.waitKey(3)	  
    i = 0
    self.difference = list()
    for i in range(len(self.list1)):
      Subs = int(self.list1[i] - self.list2[i])
      self.difference.append(abs(Subs))
    
    self.bgr_difference = list()
    for i in range(9):
         bgr = self.list3[i] - self.list4[i]
         self.bgr_difference.append(bgr)
    #print(self.difference)
    del self.list2[:]
    del self.list4[:]
    #self.list2.clear()
    #self.list4.clear()
    #print(self.bgr_difference)
    #print(self.difference)
    for i in range(9):
      for j in range(3):
	self.difference.append(self.bgr_difference[i][j])
    #print(self.bgr_difference[1][1])
    #print(self.difference)
    self.image_features.data = self.difference #+ self.bgr_difference
    self.feat_pub.publish(self.image_features)
    

    # Piece of code corresponding to contact location, which is filled in with the difference values and display contact location
    Zones = {0: [0, 100, 0, 100, self.difference[0] * 2],
         1: [100, 200, 0, 100, self.difference[1] * 2],
         2: [200, 300, 0, 100, self.difference[2] * 2],
         3: [0, 100, 100, 200, self.difference[3] * 2],
         4: [100, 200, 100, 200, self.difference[4] * 2],
         5: [200, 300, 100, 200, self.difference[5] * 2],
         6: [0, 100, 200, 300, self.difference[6] * 2],
         7: [100, 200, 200, 300, self.difference[7] * 2],
         8: [200, 300, 200, 300, self.difference[8] * 2]
         }

    s = cv2.getTrackbarPos(self.switch, 'contact')

    if s == 0:
      self.contact[:] = 0
    elif s == 1:
      for zone in Zones:
	for y in range(Zones[zone][0], Zones[zone][1]):
	  for x in range(Zones[zone][2], Zones[zone][3]):
	    self.contact[y, x] = [Zones[zone][4], Zones[zone][4], Zones[zone][4]]
	      
#    try:
#      self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
#    except CvBridgeError as e:
#      print(e)
    
    #print(self.difference + self.bgr_difference)
    
    
    
def main(args):
  rospy.init_node('rgb_sensor_node1', anonymous=True)
  ic = image_converter()
  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")
  cv2.destroyAllWindows()
  

if __name__ == '__main__':
    main(sys.argv) 

    
#---------------------------------------------    
  #def send_feature(self):
    ##feat_pub = rospy.Publisher('/image_feature', String, queue_size=10)
    ##rospy.init_node('send_feature', anonymous=True)
    ##rate = rospy.Rate(10) # 10hz
    ##print("hey")
    ##self.image_features.layout.dim = []
    ##self.image_features.layout.data_offset = 0
    
    #self.image_features.data = self.difference + self.bgr_difference
    #print(self.difference + self.bgr_difference)
    #self.feat_pub.publish(self.image_features)
    ##rospy.loginfo(self.image_features)
    ##header = rospy.Time.now()
    
    ##while not rospy.is_shutdown():
      ###hello_str = "xxx"
      ###rospy.loginfo(hello_str)
      ###Float64MultiArray image_features
      ##image_features.data = self.difference
      ##image_features.header = rospy.get_rostime()
      ##self.feat_pub.publish(image_features)
      ##rospy.loginfo(image_features)
      ##rate.sleep()
    

#def main(args):
  #rospy.init_node('rgb_sensor_node1', anonymous=True)
  #ic = image_converter()
  #while not rospy.is_shutdown():
    #ic.send_feature()
  #try:
    #rospy.spin()
  #except KeyboardInterrupt:
    #print("Shutting down")
  #cv2.destroyAllWindows()
  

#if __name__ == '__main__':
    #main(sys.argv)    